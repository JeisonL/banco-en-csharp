﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco_Narly_Practica
{
    class Clientes {
        private string Nombre;
        private double Monto;
        public Clientes(string nombre) {
            Nombre = nombre;
            Monto = 0;
        }
        public void Retirar(double a) {
            Monto -= a; // monto = monto - a;
        }
        public void Depositar(double b) {
            Monto += b; // monto = monto + b;
        }
        public double Retornar_Monto()
        {
            return Monto;
        }
        public void Imprimir()
        {
            Console.WriteLine(Nombre + " Su deposito fue de: " + Monto);
        }
    }
}
