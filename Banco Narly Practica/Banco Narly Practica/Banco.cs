﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banco_Narly_Practica
{
    class Banco
    {
        private Clientes cliente1, cliente2, cliente3;
        public Banco(string nom1, string nom2, string nom3)
        {
            cliente1 = new Clientes(nom1);
            cliente2 = new Clientes(nom2);
            cliente3 = new Clientes(nom3);
        }

        public void PintarWe()
        {
            Console.WriteLine(cliente1);
        }

        public void Operar()
        {
            cliente1.Depositar(1500000);
            cliente2.Depositar(2000000);
            cliente3.Depositar(3500000);

            cliente1.Retirar(200000);
            cliente2.Retirar(300000);
            cliente3.Retirar(100000);
        }
        public void total()
        {
            double Total = cliente1.Retornar_Monto() + cliente2.Retornar_Monto() + cliente3.Retornar_Monto();
            Console.WriteLine("El total de dinero en el banco es: " + Total);
            cliente1.Imprimir();
            cliente2.Imprimir();
            cliente3.Imprimir();
        }
    }
}
